# 3rd attempt

**I:**

Hi ChatGPT. As follows I present to you json formatted journalistic articles:

    {
        "articles": [
            {
                "paragraphs" : [
                    {
                        "text": "Die Journalist:innen dort berichten viel aus der Ich-Perspektive, werden ständig selbst im Bild gezeigt, ihre Ergriffenheit über diese oder jene Zustände ist wesentlicher Teil der Storys. „Subjektiver Journalismus“ wird das manchmal genannt. Brinkmann spricht von „haltungsgeprägtem Thesenjournalismus“."
                    },
                    {
                        "text": "Funk ist sehr erfolgreich in seiner Zielgruppe. Die Boulevardisierung ist in den Beträgen ebenso klar zu sehen wie eine Heroisierung der Journalist:innen selbst. Brinkmann bezeichnet das als „Journalismus, der manchmal wie Aktivismus aussieht“. 1.000 bis 2.000 Nutzerkommentare erhält Funk pro Sendung – eine gewaltige Zahl."
                    },
                    {
                        "text": "Und so wird bei der Tagung – Teilnehmer sind hauptsächlich Kommunikationswissenschaftler und Studierende – durchaus anerkannt, wenn junge Menschen Funk machen oder konsumieren, anstatt sich als Alternative nur noch kommerziellem Trash auf dem Handy hinzugeben. Ein Mann sagt wehm"
                    }
                ]
            }
        ],
    }

Task: Task: Classify compound nouns that describe professions, academic specializations, or roles typically associated with humans as referring to humans, even if the compound noun itself doesn't explicitly denote a human being.

1. In each field "text" identify the nouns
2. For each identified noun, make sure it refers to a human being / person. (as true or false).
3. For the remaining nouns in 2. (classified "true"), identify if it's used in its singular or plural form (as string)
4. For the nouns in 2. identify if it is used in the regular gender form, the generic masculine form, the substantive verb form, in a "gendered"-form, or in another form (as string without spaces)
5. To each field "paragraph" in the JSON in "paragraphs" add the field "tokens", which is a vector of "token"-objects
6. Only if a token classified "human": true in 2. fill the token-vector in 5. with this noun and information obtained in steps 3., 4., and 5.
7. Print the updated JSON.

Additional considerations:

- If a token contains the special character ":", or the special Character "_" or contains a capital "I" in between, classify its "form" as "gendered".
- If you are uncertain of the word token is in its singular or plural form, try to derive the information from used articles or the context of the text.

Example sentence:

    {
        ..
        "text": "Forschende haben herausgefunden, dass lange Ringfinger auf eine erhöhte Spermienmobilität bei echten Männern hinweisen".
        ..
    },

Example of added fields:

    {
    ..
        "tokens": [
            {
                "token": "Forschende",
                "quantity": "plural",
                "human": true,
                "form": "substantive verb"
            },
            {
                "token": "Männer",
                "quantity": "plural",
                "human": true,
                "form": "regular"
            }
        ]
    }

Finalize:

For each step of 2., 3., 4. present one example, and briefly describe the rationale behind your decision. Present this as a bullet-point list.


**ChatGPT:**

    {
        "articles": [
            {
                "paragraphs": [
                    {
                        "text": "Die Journalist:innen dort berichten viel aus der Ich-Perspektive, werden ständig selbst im Bild gezeigt, ihre Ergriffenheit über diese oder jene Zustände ist wesentlicher Teil der Storys. „Subjektiver Journalismus“ wird das manchmal genannt. Brinkmann spricht von „haltungsgeprägtem Thesenjournalismus“.",
                        "tokens": [
                            {
                                "token": "Journalist:innen",
                                "human": true,
                                "quantity": "plural",
                                "form": "gendered"
                            },
                            {
                                "token": "Brinkmann",
                                "human": true,
                                "quantity": "singular",
                                "form": "regular"
                            }
                        ]
                    },
                    {
                        "text": "Funk ist sehr erfolgreich in seiner Zielgruppe. Die Boulevardisierung ist in den Beträgen ebenso klar zu sehen wie eine Heroisierung der Journalist:innen selbst. Brinkmann bezeichnet das als „Journalismus, der manchmal wie Aktivismus aussieht“. 1.000 bis 2.000 Nutzerkommentare erhält Funk pro Sendung – eine gewaltige Zahl.",
                        "tokens": [
                            {
                                "token": "Funk",
                                "human": true,
                                "quantity": "singular",
                                "form": "regular"
                            },
                            {
                                "token": "Journalist:innen",
                                "human": true,
                                "quantity": "plural",
                                "form": "gendered"
                            },
                            {
                                "token": "Brinkmann",
                                "human": true,
                                "quantity": "singular",
                                "form": "regular"
                            }
                        ]
                    },
                    {
                        "text": "Und so wird bei der Tagung – Teilnehmer sind hauptsächlich Kommunikationswissenschaftler und Studierende – durchaus anerkannt, wenn junge Menschen Funk machen oder konsumieren, anstatt sich als Alternative nur noch kommerziellem Trash auf dem Handy hinzugeben. Ein Mann sagt wehm",
                        "tokens": [
                            {
                                "token": "Tagung",
                                "human": false
                            },
                            {
                                "token": "Kommunikationswissenschaftler",
                                "human": true,
                                "quantity": "plural",
                                "form": "regular"
                            },
                            {
                                "token": "Studierende",
                                "human": true,
                                "quantity": "plural",
                                "form": "regular"
                            },
                            {
                                "token": "Mann",
                                "human": true,
                                "quantity": "singular",
                                "form": "regular"
                            }
                        ]
                    }
                ]
            }
        ]
    }
