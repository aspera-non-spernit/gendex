
# First attempt

**I:**

    Here is an example excerpt of a text corpus in json:

    {
  	    "articles": [
            {
                "paragraphs" : [
              	    {
                        "text": "Die Journalist:innen dort berichten viel aus der Ich-Perspektive, werden ständig selbst im Bild gezeigt, ihre Ergriffenheit über diese oder jene Zustände ist wesentlicher Teil der Storys. „Subjektiver Journalismus“ wird das manchmal genannt. Brinkmann spricht von „haltungsgeprägtem Thesenjournalismus“."
                    },
              	    {
                        "text": "Funk ist sehr erfolgreich in seiner Zielgruppe. Die Boulevardisierung ist in den Beträgen ebenso klar zu sehen wie eine Heroisierung der Journalist:innen selbst. Brinkmann bezeichnet das als „Journalismus, der manchmal wie Aktivismus aussieht“. 1.000 bis 2.000 Nutzerkommentare erhält Funk pro Sendung – eine gewaltige Zahl."
                    },
        		    {
                        "text": "Und so wird bei der Tagung – Teilnehmer sind hauptsächlich Kommunikationswissenschaftler und Studierende – durchaus anerkannt, wenn junge Menschen Funk machen oder konsumieren, anstatt sich als Alternative nur noch kommerziellem Trash auf dem Handy hinzugeben. Ein Mann sagt wehm"
                    }
                ]
            }
        ],
	    "help": ["Journalist", "Teilnehmer", "Forscher", "Student"]
    }

    It contains an article separated into paragraphs, which contain the text of the article. The field "help" contains a vector ofoften 'gendered' words, and which need extra attention. You may want to check twice, if one of these words (given in the form of the generic masculine form) are found in their 'gendered' form in the text. 

    **Task:** Analyze the text corpus
    **Steps:**
    1. Tokenize each paragraph
    2. Add a field "tokens" which is a word vector to "paragraphs" containing all word tokens
    3. Add a field "gendered" which is word vector to "paragraphs" containing all gendered word used in the paragraphs
    4. Calculation of a gender strength index: Use "number of gendered tokens per parapraph * 1000 / number of tokens" to set the number of gendered words into relation to the size of the text corpus.
    5. Add a field "gender_index" to the field "articles" containing the result of the computation in 3. as a float value
    6. Calculation of text sentiment. Execute a sentiment analysis for each paragraph, classifying negative, neutral positive. Choose the best fitting algorithm you are capable of executing.
    7. Add a field "sentiment" to each paragraph-object, containing the result of your sentiment analysis
    **Output format:** json

**ChatGPT:**

    {
    "articles": [
        {
        "paragraphs": [
            {
            "text": "Die Journalist:innen dort berichten viel aus der Ich-Perspektive, werden ständig selbst im Bild gezeigt, ihre Ergriffenheit über diese oder jene Zustände ist wesentlicher Teil der Storys. „Subjektiver Journalismus“ wird das manchmal genannt. Brinkmann spricht von „haltungsgeprägtem Thesenjournalismus“.",
            "tokens": ["Die", "Journalist:innen", "dort", "berichten", "viel", "aus", "der", "Ich-Perspektive", "werden", "ständig", "selbst", "im", "Bild", "gezeigt", "ihre", "Ergriffenheit", "über", "diese", "oder", "jene", "Zustände", "ist", "wesentlicher", "Teil", "der", "Storys", "Subjektiver", "Journalismus", "wird", "das", "manchmal", "genannt", "Brinkmann", "spricht", "von", "haltungsgeprägtem", "Thesenjournalismus"],
            "gendered": ["Die", "Journalist:innen"],
            "sentiment": "neutral"
            },
            {
            "text": "Funk ist sehr erfolgreich in seiner Zielgruppe. Die Boulevardisierung ist in den Beträgen ebenso klar zu sehen wie eine Heroisierung der Journalist:innen selbst. Brinkmann bezeichnet das als „Journalismus, der manchmal wie Aktivismus aussieht“. 1.000 bis 2.000 Nutzerkommentare erhält Funk pro Sendung – eine gewaltige Zahl.",
            "tokens": ["Funk", "ist", "sehr", "erfolgreich", "in", "seiner", "Zielgruppe", "Die", "Boulevardisierung", "ist", "in", "den", "Betragen", "ebenso", "klar", "zu", "sehen", "wie", "eine", "Heroisierung", "der", "Journalist:innen", "selbst", "Brinkmann", "bezeichnet", "das", "als", "Journalismus", "der", "manchmal", "wie", "Aktivismus", "aussieht", "1.000", "bis", "2.000", "Nutzerkommentare", "erhält", "Funk", "pro", "Sendung", "eine", "gewaltige", "Zahl"],
            "gendered": ["Die", "Journalist:innen"],
            "sentiment": "positive"
            },
            {
            "text": "Und so wird bei der Tagung – Teilnehmer sind hauptsächlich Kommunikationswissenschaftler und Studierende – durchaus anerkannt, wenn junge Menschen Funk machen oder konsumieren, anstatt sich als Alternative nur noch kommerziellem Trash auf dem Handy hinzugeben. Ein Mann sagt wehmütig: „Wer liest denn noch Zeitungen wie die edle FAZ?“ Wenn vorwiegend Alt über Jung spricht, so schwingt auch eine gewisse Herablassung mit.",
            "tokens": ["Und", "so", "wird", "bei", "der", "Tagung", "Teilnehmer", "sind", "hauptsächlich", "Kommunikationswissenschaftler", "und", "Studierende", "durchaus", "anerkannt", "wenn", "junge", "Menschen", "Funk", "machen", "oder", "konsumieren", "anstatt", "sich", "als", "Alternative", "nur", "noch", "kommerziellem", "Trash", "auf", "dem", "Handy", "hinzugeben", "Ein", "Mann", "sagt", "wehmütig", "Wer", "liest", "denn", "noch", "Zeitungen", "wie", "die", "edle", "FAZ", "Wenn", "vorwiegend", "Alt", "über", "Jung", "spricht", "so", "schwingt", "auch", "eine", "gewisse", "Herablassung", "mit"],
            "gendered": ["Teilnehmer", "Mann"],
            "sentiment": "neutral"
            }
        ]
        }
    ],
    "help": ["Journalist", "Teilnehmer", "Forscher", "Student"],
    "gender_index": 166.66666666666666
    }

## Analysis

ChatGPT

- understands the instructions,
- does not get the importance of singular and plural.
- adds the articles to gendered words,
- may add ungendered words (ie. Mann) to the gendered word vector

