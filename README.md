# gendex

The anti-woke text analytics and manipulation tool (in development) and learning in process.

**Update** 

This program was probably overtaken by the rapid development of GPT.

I am experimenting with ChatGPT in the future to develop an AI-powered anti-woke agent. [See here](ChatGPT.md)

## modules

All top level modules are declared optional features with the same name.

- preproc
- algo
- net
- utils

### preproc

The trait `Tokenize<Document, Tokenized>` is generic over the passed `Document` and the format of the `Tokenized` documemnt.

Implementors may have delimiters. Two sets of common delimiters for this specific purpose are provided by the constants `DEFAULT_DELIMITERS` and `INTERNAL_I_DELIMITERS` in the `preproc::tokenizer` module.

There are two example Tokenizer implementations available that reliably tokenize almost all gender neutral german texts, including rarely used *ï*.

- `RegexTokenizer<String, Vec<String>>` uses a simple raw string as delimiters that are consumed (removed) in the result.

```rust
Kommunalpolitiker|[iI]nnen -> ["Kommunalpolitiker", "[iI]Innen"]
Bürger_[iI]nnen -> ["Bürger", "[iI]nnen"]
Sehr geehrtx Profex -> ["Sehr", "geehrt", "Profe"]
Politiker:[iI]nnen -> ["Politiker", "[iI]nnen"]
..
```

- `InternalITokenizer<String, Vec<String>>` can be used to tokenize words that contain so called internal capialized I-characters often seen in gender independet texts in German language. The result retains the matched patterns. It's advised to run this after the RegexTokenizer, that may produce uncomplete results.

**Examples:**

```rust
RikschafahrerIn -> ["Rikschafahrer", "In"]
InnenarchitektInnen -> ["Innenarchitekt", "Innen"]
ExpertInnenkommission -> ["Expert", "Innen", "Kommission"]
Studierenden -> ["Studier", "enden"]
Radfahrende -> ["Radfahr", "ende"]
DänInnen danken der Indoorfahradfahrendengruppe -> ["Dän", "Innen", "danken", "der", "Indoorfahradfahr", "enden", "gruppe"]

but not

ansteckende -> ["ansteckende"]
Endemie -> ["Endemie"]
Endentscheidung -> ["Endentscheidung"]
immerwährende -> ["immerwährende"]
```

In some cases, the tokenizers may not produce the desired result. For instance, the document may speak about both genders but uses the generic female form. Here a delimiter such as "innen" may or may not be applied:

```rust
// Syntactically tokenized correctly. Maybe not sematically
Mitarbeiterinnen -> ["Mitarbeiter", "innen"]
// Syntactically tokenized incorrectly the words "Finnen" and "pinnen"
Die Finnen pinnen BärInnen -> ["Die", "F", "innen", "p", "innen", "Bär", "Innen"]
// Evertyhing is tokenized correctly
Däninnen danken der Indoorfahradfahrendengruppe -> ["Dän", "innen", "danken", "der", "Indoofahrradfahr", "enden", "gruppe"]
```

However, if desired, you may just not apply certain delimiters such as ```innen``` by using your own set of delimiters or filter that doesn't apply to the tokenizer to certain words:

```rust
// All words containing innen will not be tokenized although the delimiter contains the pattern "innen"
if !word.contains("innen") { .. }
// Maybe used as the generic female form, then should have been tokenized
Mitarbeiterinnen -> ["Mitarbeiterinnen"]
Die Finnen pinnen BärInnen -> ["Die", "Finnen", "pinnen", "Bär", "Innen"]
Däninnen danken der Frauen-Indoorfahradfahrendengruppe -> ["Däninnen", "danken", "der", "Frauen", "Indoofahrradfahr", "enden", "gruppe"]
```

At the moment a workaround is used in testing, that eliminates some of the errors and may be pushed into the crate in the future, as "RuleSet"

```rust
if word.contains("In") && !word.starts_with("In")
    || word.contains("Innen") && !word.starts_with("Innnen") 
    || word.ends_with("Innen")
    || word.contains("ïnnen")
    || word.ends_with("ende") && word.chars().nth(0).unwrap().is_uppercase()
    || word.ends_with("enden") && word.chars().nth(0).unwrap().is_uppercase()

Eine ansteckende Endemie infiziert alle Politiker:innen, 
finnische InnenarchitektInnen,
Däninnen und auf Binnengewässern, die Radfahrenden.
-> [
    "Eine",
    "ansteckende", // *ende
    "Endemie", // Ende*
    "infiziert",
    "alle",
    "Politiker", // :innen
    "innen",
    "finnische",
    "Innenarchitekt", // *Innen
    "Innen",
    "Däninnen", // *innen
    "und",
    "auf"
    "Binnengewässern" // *innen*
    "die"
    "Radfahr", // *enden
    "enden"
]
```

This drawback however, is probably negligible for the further ngrams or tf-idf processing, clustering or classification. Therefore words containing "innen" should (even incorrectly) tokenized.

For large sets you may want to check a word (string) for a given delimiter before using the tokenizers.

The trait `Ngram` is generic over the provided Document and the `Out` format. The single associated function `ngrams` implemented in the unit struct `Ngrams` takes a `&[String]` and returns a `Vec<Vec<String>>`.

#### state

- [x] tokenizer
- [] denoiser
- [x] ngrams
- [] convert (ie. html into markdown, after net module is ready)

### algo

A selection of rules based and intelligent algorithms for the clusteriing and classification of preprocessed documents.

In the first step, the idea is use k-means to clusterize tf-idf weighted ngrams from preprocessd documents:

```rust

Document > Tokenized Document > Ngrams > tf-idf > k-means

```

Naive-Bayes may work too, but since the vast majority of documents is unlabelled, this may be unfeasible.

#### state

- [] Navive-Bayes
- [] k-means
- [] tf-idf

### net

Fetch documments from the web and provide gendex as a service.

#### state

- [] fetch documents from the web
- [] gendex as a service
