#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(dead_code)]
#[cfg(feature = "algo")]
pub mod algo;
#[cfg(feature = "io")]
pub mod io;
#[cfg(feature = "net")]
pub mod net;
#[cfg(feature = "preproc")]
pub mod preproc;
#[cfg(feature = "utils")]
pub mod utils;

use serde::{Serialize, Deserialize};
use std::collections::HashSet;

#[derive(Debug, Deserialize, Serialize)]
pub enum JobType { Visit, Scrape }

pub trait Execute<JobResult, JobError> {
    fn execute(&self) -> Result<JobResult, JobError>;
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Job {
    pub base: String,
    pub anchors: HashSet<String>,
    pub anchor_pat: String,
    pub article_pat: String,
    pub exclude_visited: HashSet<String>,
    pub exclude_pat: Vec<String>,
}