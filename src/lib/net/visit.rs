use crate::{
    Execute, Job, JobType,
    net::{ DateTime, Document, FixedOffset, Prepare, Post }
};
pub use reqwest::{ Client, Error, Response };
pub use tokio::runtime::Runtime;
use std::{
    collections::HashSet,
    result::Result
};

#[derive(Debug)]
pub struct VisitJob {
    client: String,
}

#[derive(Debug)]
pub struct Visitor { pub job: Job }

// pub struct Visitor { pub job: VisitJob }

impl Execute<std::vec::IntoIter<Document<String, FixedOffset, String> >, Box<dyn std::error::Error> > for Visitor {

    #[tokio::main(flavor = "multi_thread")]
    async fn execute(&self) -> Result< std::vec::IntoIter<Document<String, FixedOffset, String> >, Box<dyn std::error::Error> > {
        // compile mutable work vector

        let mut todo: HashSet<String> = self.job.anchors.iter().cloned().collect();

        // apply guards and remove excluded urls from todo vec
        self.job.anchors.iter().enumerate().for_each(|(i, url)| {
            // calling guards
            if true == self.prepare(&url) {
                todo.remove(url);
                println!("removing from todo: {} {}", i, url);
            }
        });

        // create mutable clone of provided vec of visited urls
        let mut visited = self.job.exclude_visited.iter()
            .cloned()
            .map(|s|s)
            .collect::<Vec<String>>();

        // create client for job
        let client = Client::builder()
            .user_agent("Mozilla/5.0 (Macintosh; Intel Mac OS X 11_3_2; rv:106.0) Gecko/20000101 Firefox/106.0")
            .build().unwrap();
            
        let mut docs = vec![]; // visited pages to return

        // iterate over anchors to visit
        for (i, url) in todo.iter().enumerate() {
            let fut_resp = client.get(url)
            .send()
            .await?;
          
            // add url to visited
            visited.push(url.to_string()); // try &str
            let hm = fut_resp.headers().clone();

            let date = hm.get("date").expect("Header got no date field");
            let date = date.to_str().expect("Couldn't make a str from header field.");
            let date = DateTime::parse_from_rfc2822(date).ok();
      
            docs.push (
                Document {
                    author: None,
                    title: None,
                    content: fut_resp.text().await?.to_string(),
                    date,
                    source: Some(url.to_owned()),
                }
            );
        }
        Ok( docs.into_iter() )
    }
}

// Prepare used as guards (filtering urls)
impl Prepare<&str, bool> for Visitor {
    fn prepare(&self, url: &str) -> bool {
        let guard = false;
        // guards
        // filtering excluded urls
        if url.contains("#bb") {
            // println!("found pattern: {}", url);
            return true
        }

        // filtering already visited pages
        if let Some(_) = self.job.exclude_visited.iter().find(|v| v == &url) {
            println!("already visited: {}", url);
            return true
        }
        guard
    }
}

impl Post<Vec<String>> for Visitor {
    fn finish(&self, todo: Vec<String>) {
        println!("saving {:?} to file", todo);
    }
}