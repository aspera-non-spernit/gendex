use crate::net::{ Document,  FixedOffset, Local };

pub use scraper::{ node::Element, ElementRef,  Html, Selector};
pub use url::{ Url, ParseError };
use std::time::Duration;
pub struct AnchorScraper;
pub struct ContentScraper;

pub trait Scrape<Document, Select, Parsed> { fn scrape(document: Document, select_pattern: Select) -> Parsed; }

pub struct ScrapeJob {
    pub document: Document<String, FixedOffset, String>,
    pub base_url: Url,
    pub elem: String,
    // pub delay: Duration,
    pub exlude_patterns: Vec<String>,
}


// impl Scrape<&Document<String, FixedOffset, String>, Result<Document<Vec<String>, Local, Url>, Box<dyn std::error::Error>>> for AnchorScraper {
impl <'a>Scrape<ScrapeJob, &'a str, Result<Document<Vec<String>, Local, Url>, Box<dyn std::error::Error>>> for AnchorScraper {
    fn scrape(scrape_job: ScrapeJob, select_pattern: &'a str) -> Result<Document<Vec<String>, Local, Url>, Box<dyn std::error::Error>> {

        let url = Url::parse(scrape_job.document.source.as_ref().unwrap())?;
        let base = format!("{}://{}", url.scheme(), url.host().unwrap() );

        let document = Html::parse_document(&scrape_job.document.content);
        // links
        let anchor_select = Selector::parse("a").unwrap();
        
      

        let content = document.select(&anchor_select)
            .map(|a| a.value().attr("href") )
            .flatten()
            .map(|a| {
                if a.starts_with('/') && a.contains('!') && !a.contains('.') {
                    Some( format!("{}{}", base, a.to_owned() ) )
                } else { None }
            } )
            .flatten()
            .collect();
        Ok(
            Document {
                author: Some("AnchorScraper".to_string()),
                title: Some("anchors list".to_string()),
                content,
                date: Some( Local::now() ),
                source: Some (url)
            }
        )
    }
}

impl <'a>Scrape<&'a str, &'a str, Result<Document<Vec<String>, FixedOffset, String>, Box<dyn std::error::Error>>> for ContentScraper {
    fn scrape(html: &'a str, select_pattern: &'a str) -> Result<Document<Vec<String>, FixedOffset, String>, Box<dyn std::error::Error>> {
        let document = Html::parse_document(html.as_ref());

        // author
        let selector = Selector::parse("meta").unwrap();
        let author = if let Some(author_elem) = document.select(&selector).find(|meta_elem| {
            meta_elem.value().attr("name") == Some("author") // is there a meta tag with name author
        }) {
            Some ( author_elem.value().attr("content").unwrap().to_owned() ) // &str to String
        } else { None };
      
        // title
        let selector = Selector::parse("title").unwrap();
        let title: String = document.select(&selector).map(|e| {
            e.inner_html()
        }).collect::<Vec<String>>()[0].clone();
        
        // article
        let selector = Selector::parse(select_pattern).unwrap(); // "p.article" | article > section > div > p
       
        let content: Vec<String> = document.select(&selector).map(|p| {
            let frag = Html::parse_fragment( &p.inner_html() );
            let mut text = String::new();
            for node in frag.tree {
                if let scraper::node::Node::Text(t) = node {
                    text.push_str(&t);
                }
            }
            text
        }).collect();
     
        Ok(
            Document {
                author,
                title: Some(title),
                content,
                date: None,
                source: None
            }
        )
    }
}