pub mod scrape;
pub mod visit;
pub use chrono::{ DateTime, FixedOffset, Local, TimeZone }; 

#[derive(Debug)]
pub struct Document<Content, Tz: chrono::TimeZone, Source> {
    pub author: Option<String>,
    pub title: Option<String>,
    pub content: Content,
    pub date: Option<DateTime<Tz>>,
    pub source: Option<Source>
}

pub trait Prepare<Material, Prepped> {
    fn prepare(&self, mat: Material) -> Prepped;
}

pub trait Post<Material> {
    fn finish(&self, mat: Material);
}