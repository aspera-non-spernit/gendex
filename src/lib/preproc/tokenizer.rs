pub use regex::*;
use crate::utils::split_and_join;

pub struct InternalITokenizer<'a> { pub delims: &'a[&'a str] }
pub struct RegexTokenizer { pub delims: String }
pub struct JoinTokenizer<'a> { pub delims: &'a[&'a str] }

pub const DEFAULT_DELIMITERS: &str = r"[0-9]|[\s\r\n\t\.\+\*\?\^\$\(\)\[\]\{\}\|\\]|[!/$&:_;,-=#'<>%~„“]|x\b|–";  // taz: 
pub const SPLIT_JOIN_DELIMITERS: &[&str] = &["\u{ad}"];
pub const INTERNAL_I_DELIMITERS: &[&str] = &["Innen", "ïnnen", "innen", "In", "enden", "ende"];
pub trait Tokenize<Document, Tokenized> {
    fn tokenize(&self, document: Document) -> Tokenized;
}

// splits and joins word 
impl <'a>Tokenize<&'a str, String> for JoinTokenizer<'a> {
    fn tokenize(&self, word: &'a str) -> String {
        SPLIT_JOIN_DELIMITERS.iter().map(|delim| -> String {
            if word.contains(delim) {
                split_and_join(word, delim)
            } else {
                word.to_string()
            }
        }).collect::<String>()
    }
}
// split document by regex delimiters, then check if split words contain split-join characters,
// if yes, split and join into string.
impl <'a>Tokenize<&'a str, Vec<String>> for RegexTokenizer {
    fn tokenize(&self, document: &'a str) -> Vec<String> {
        let reg = Regex::new(&self.delims).unwrap();
        let mut split: Vec<String> = reg.split(&document)
            .collect::<Vec<&str>>()
            .iter()
            .map(|s| -> String {
                s.to_string()
            }).collect();
        split.retain(|e| e != "" );
        split
    }
}

impl <'a>Tokenize<&'a str, Vec<String>> for InternalITokenizer<'a> {
    fn tokenize(&self, word: &'a str) -> Vec<String> {
        let mut split = vec![];

        self.delims.iter().for_each(|delim| {
            if word.contains(delim) {
                if let Some(pos) = word.rfind(delim) {
                    let mut sp = word.split_at( pos );
                    split = vec![sp.0.to_string(), sp.1.to_string()];
                    // difficult to split (ie. ExpertInnenkommision), first split: split[0] = Expert, split[1] = Innenkommission
                    // needs 2nd split at known pos
                    if split[1].starts_with(delim) && split[1].ne( delim ) {
                        sp = split[1].split_at(5);
                        split = vec![split[0].clone(), sp.0.to_string(), sp.1.to_string()];
                    }
                }
            }
        });
        // split may contain empty entries
        split.retain(|w| w != ""); 
        split
    }
}

