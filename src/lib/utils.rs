#[macro_export]
macro_rules! load_to_vec {
    ($file:expr) => {
        match File::open($file) {
            Ok(f) => {
                let content = BufReader::new(f).lines().map(|l| {
                    if let Ok(line) = l {
                        Some (line)
                    } else {
                        None
                    }
                })
                .flatten()
                .collect::<Vec<String>>();
                Ok ( content )
            },
            Err(e) => { Err (e) }
        }
    }
}

#[macro_export]
macro_rules! load {
    ($file:expr) => {
        std::fs::read_to_string($file).expect("Cannot read file.")
    }
}

#[macro_export]
macro_rules! load_jobs {
    ($file:expr) => {
        
        serde_json::from_str(&load!($file))
       
    }
}
pub fn split_and_join<'a>(word: &'a str, delim: &'a str) -> String {
    word.split(delim).collect::<Vec<&str>>().join("")
}