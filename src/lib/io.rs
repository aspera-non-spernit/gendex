// re-exports
pub use serde::{ Deserialize, Serialize };

pub use serde_json;

#[derive(Debug, Deserialize, Serialize)]
pub struct DocInfo {
    pub author: String,
    pub source: String,
    pub title: String,
}

impl std::convert::From<&[String]> for DocInfo {
    fn from(str: &[String]) -> Self {
        let iter = str.split(|line| line.eq("---") );
        iter.for_each(|i| { println!("{:#?}", i); } );
        
        DocInfo {
            author: "".to_string(),
            source: "".to_string(),
            title: "".to_string()
        }
    }
}
