pub use classifier; // Re-export of NaiveBayes

pub trait Classify {
    type C;
    fn classify(&self, content: Self::C) -> String;
}

pub trait Train {
    type T;
    fn train(&mut self, content: Self::T);
}